# 建立基本 alpine 的 Python 3.5
docker build -t ithome_pellok_2018:base .

# 儲存到私有昌庫
docker tag ithome_pellok_2018:base asia.gcr.io/tidal-surf-219013/ithome_pellok_2018:base
docker push asia.gcr.io/tidal-surf-219013/ithome_pellok_2018:base

