ithome_pellok_2018 README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/initialize_ithome_pellok_2018_db development.ini

- $VENV/bin/pserve development.ini

