#!/bin/bash
# 套用環境變數
. ~/.bash_profile
# 檢查pyenv
which pyenv
if [ $? == "0" ]; then
    echo "already have pyenv"
else
    echo ""
    git clone git://github.com/yyuu/pyenv.git ~/.pyenv
    echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> ~/.bash_profile
    echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
    . ~/.bash_profile
    pyenv install 3.5.0
    pyenv global 3.5.0
    pip install --upgrade pip setuptools virtualenv
fi
# 設定專案
PYENV_HOME="`pwd`/env"
echo ${PYENV_HOME}
if [ -d ${PYENV_HOME} ]; then
    . ${PYENV_HOME}/bin/activate
else
    pyenv global 3.5.0
    virtualenv ${PYENV_HOME}
    . ${PYENV_HOME}/bin/activate
    # 準備專案相依插件
    pip install cryptography psycopg2 SQLAlchemy psycopg2-binary
    python setup.py develop
    pip install -e ".[testing]"
fi
# 初始化 DB
rm -f ithome_pellok_2018.sqlite
${PYENV_HOME}/bin/initialize_ithome_pellok_2018_db production.ini#ithome_pellok_2018
