# This dockerfile uses the python pyramid
# VERSION 1 - EDITION 1
# Author: pellok
# Command describe

# 使用的python映像檔版本
FROM python:3.5

MAINTAINER pellok pellok@okborn.com

# 創建存放專案的資料夾
RUN mkdir -p /usr/src/app

# 複製當前目錄的所有檔案到容器內的，資料放在/usr/src/app
COPY . /usr/src/app

# 指定工作目錄
WORKDIR /usr/src/app/

RUN pip install --upgrade pip setuptools
RUN pip install --no-cache-dir -r requirement.txt

# 安裝環境變數和相依性套件
RUN python setup.py develop

# 初始化DB
#RUN initialize_ithome_pellok_2018_db development.ini

# 專案監聽的Port號
EXPOSE 6543

# 啟動專案
CMD pserve development.ini