import transaction
import unittest
import webtest
from ithome_pellok_2018.models.meta import Base
from ithome_pellok_2018.models import get_tm_session
from ithome_pellok_2018 import main

class FunctionalTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        settings = {
            'sqlalchemy.url': 'sqlite://',
            'auth.secret': 'seekrit',
        }
        app = main({}, **settings)
        cls.testapp = webtest.TestApp(app)

        session_factory = app.registry['dbsession_factory']
        cls.engine = session_factory.kw['bind']
        Base.metadata.create_all(bind=cls.engine)

        with transaction.manager:
            dbsession = get_tm_session(session_factory, transaction.manager)

    @classmethod
    def tearDownClass(cls):
        Base.metadata.drop_all(bind=cls.engine)

    def test_home(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Hello World Mock Server' in res.body)