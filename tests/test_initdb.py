import os
import unittest
from ithome_pellok_2018.scripts.initializedb import main

class TestInitializeDB(unittest.TestCase):

    def test_usage(self):

        with self.assertRaises(SystemExit):
            main(argv=['ithome_pellok_2018'])

    def test_run(self):
        path = os.path.abspath(os.path.join(os.path.dirname("__file__")))
        os.remove('{}/ithome_pellok_2018.sqlite'.format(path))
        main(argv=['ithome_pellok_2018',
                   '{}/development.ini'.format(path)])
        self.assertTrue(os.path.exists('{}/ithome_pellok_2018.sqlite'.format(path)))
