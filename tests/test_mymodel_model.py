import unittest
import transaction

from pyramid import testing

from ithome_pellok_2018.models import (
            get_tm_session,
            )
from ithome_pellok_2018.models.meta import Base
from ithome_pellok_2018.models import MyModel


class BaseTest(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('ithome_pellok_2018.models')

        session_factory = self.config.registry['dbsession_factory']
        self.session = get_tm_session(session_factory, transaction.manager)

        self.init_database()

    def init_database(self):
        session_factory = self.config.registry['dbsession_factory']
        engine = session_factory.kw['bind']
        Base.metadata.create_all(engine)

    def tearDown(self):
        testing.tearDown()
        transaction.abort()

    def create_mymodel(self, name, value):
        return MyModel(name=name, value=value)

class TestMymodel(BaseTest):

    def test__saved(self):
        name = 'foo'
        value = 'bar'
        mymodel = self.create_mymodel(name=name, value=value)

        self.assertEqual(mymodel.name, name)
        self.assertEqual(mymodel.value, value)