import unittest
import transaction

from pyramid import testing
from ithome_pellok_2018.models import (
            get_engine,
            get_session_factory,
            get_tm_session,
            )
from ithome_pellok_2018.models.meta import Base
from ithome_pellok_2018.models import MyModel
from ithome_pellok_2018.views.api.api import ApiView

def dummy_request(dbsession):
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('ithome_pellok_2018.models')
        settings = self.config.get_settings()
        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)
        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)


class TestApiView(BaseTest):

    def setUp(self):
        super(TestApiView, self).setUp()
        self.init_database()

    def create_mymodel(self, name='one', value='123'):
        """
        創建 Table 資料
        """
        mymodel = MyModel(name=name, value=value)
        self.session.add(mymodel)
        mymodel = self.session.query(MyModel)\
                      .filter(MyModel.name == name).first()
        return mymodel

    def test_create(self):
        """
        測試 創建 API 資料
        """
        request = dummy_request(self.session)
        name = 'one'
        value = 'test'
        request.params = {'name': name, 'value': value}

        api_view = ApiView(request)
        info = api_view.create()
        self.assertEqual(info.get('code', 500), 200)
        self.assertEqual(info.get('status', False), True)
        self.assertEqual(info.get('response', {}).get('name'), name)
        self.assertEqual(info.get('response', {}).get('value'), value)

        return info.get('response', {})

    def test_search(self):
        """
        測試 搜尋 API 資料
        """
        request = dummy_request(self.session)
        api_view = ApiView(request)
        info = api_view.search()
        self.assertEqual(info.get('code', 500), 200)
        self.assertEqual(info.get('status', False), True)
        self.assertEqual(len(info.get('response')), 0)


    def test_get(self):
        """
        測試 讀取 API 資料
        """
        mymodel = self.create_mymodel()

        request = dummy_request(self.session)
        request.matchdict['id'] = mymodel.id
        api_view = ApiView(request)
        info = api_view.get()
        self.assertEqual(info.get('code', 500), 200)
        self.assertEqual(info.get('status', False), True)
        self.assertEqual(info.get('response', {}).get('name'), mymodel.name)
        self.assertEqual(info.get('response', {}).get('value'), mymodel.value)

    def test_update(self):
        """
        測試 更新 API 資料
        """
        mymodel = self.create_mymodel()

        request = dummy_request(self.session)
        request.matchdict['id'] = mymodel.id
        name = 'one'
        value = 'test'
        request.params = {'name': name, 'value': value}
        api_view = ApiView(request)
        info = api_view.edit()
        self.assertEqual(info.get('code', 500), 200)
        self.assertEqual(info.get('status', False), True)
        self.assertEqual(info.get('response', {}).get('name'), name)
        self.assertEqual(info.get('response', {}).get('value'), value)


    def test_delete(self):
        """
        測試 刪除 API 資料
        """
        mymodel = self.create_mymodel()

        request = dummy_request(self.session)
        request.matchdict['id'] = mymodel.id
        api_view = ApiView(request)
        info = api_view.delete()
        self.assertEqual(info.get('code', 500), 200)
        self.assertEqual(info.get('status', False), True)
        self.assertEqual(info.get('response'), 'OK')
