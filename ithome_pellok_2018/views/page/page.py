# coding=utf8
from __future__ import unicode_literals
from pyramid.response import Response

class PageView(object):
    def __init__(self, request):
        super(PageView, self).__init__()
        self.request = request

    def home(self):
        """
        首頁
        """
        try:
            return {}
        except Exception as e:
            return Response(str(e), content_type='text/plain', status=500)




