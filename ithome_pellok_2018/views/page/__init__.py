from . import page

def includeme(config):
    config.add_route('page.home', '/')
    config.add_view(
        page.PageView, attr='home',
        route_name='page.home',
        renderer='../../templates/home.jinja2'
    )

