# coding=utf8
from __future__ import unicode_literals

def includeme(config):
    config.include('.api')
    config.include('.page')
