from . import api

def includeme(config):
    config.add_route('api', '/api')
    config.add_view(
        api.ApiView, attr='create',
        route_name='api',
        request_method='POST',
        renderer='json'
    )
    config.add_view(
        api.ApiView, attr='search',
        route_name='api',
        request_method='GET',
        renderer='json'
    )

    config.add_route('api.info', '/api/{id}')
    config.add_view(
        api.ApiView, attr='get',
        route_name='api.info',
        request_method='GET',
        renderer='json'
    )

    config.add_view(
        api.ApiView, attr='edit',
        route_name='api.info',
        request_method='POST',
        renderer='json'
    )

    config.add_view(
        api.ApiView, attr='delete',
        route_name='api.info',
        request_method='DELETE',
        renderer='json'
    )

