# coding=utf8
from __future__ import unicode_literals
import transaction
from ...models import MyModel

class ApiView(object):
    def __init__(self, request):
        super(ApiView, self).__init__()
        self.request = request
        self.dbsession = self.request.dbsession

    def create(self):
        """
        創建 API
        """
        try:
            name = self.request.params.get('name')
            value = self.request.params.get('value')

            with transaction.manager:
                self.dbsession.add(MyModel(name=name, value=value))

            mymodel = self.dbsession.query(MyModel).\
                           filter(MyModel.name == name).first()

            return {'code': 200, 'status': True,
                    'response': {'id': mymodel.id,
                                 'name': mymodel.name,
                                 'value': mymodel.value}}

        except Exception as e:
            return {'code': 500, 'status': False, 'response': str(e)}


    def search(self):
        """
        搜尋 API
        """
        try:
            mymodel_list = self.dbsession.query(MyModel).all()
            mymodel_json_list = []
            for mymodel in mymodel_list:
                mymodel_json_list.append({'id': mymodel.id,
                                          'name': mymodel.name,
                                          'value': mymodel.value})

            return {'code': 200, 'status': True,
                    'response': mymodel_json_list}

        except Exception as e:
            return {'code': 501, 'status': False, 'response': str(e)}


    def get(self):
        """
        讀取 API
        """
        try:
            mymodel = self.dbsession.query(MyModel)\
                          .filter(MyModel.id == int(self.request.matchdict['id']))\
                          .first()

            return {'code': 200, 'status': True,
                    'response': {'id': mymodel.id,
                                 'name': mymodel.name,
                                 'value': mymodel.value}}

        except Exception as e:
            return {'code': 501, 'status': False, 'response': str(e)}

    def edit(self):
        """
        編輯 API
        """
        try:
            name = self.request.params.get('name')
            value = self.request.params.get('value')

            mymodel = self.dbsession.query(MyModel)\
                          .filter(MyModel.id == int(self.request.matchdict['id']))\
                          .first()

            with transaction.manager:
                mymodel.name = name
                mymodel.value = value
                self.dbsession.flush()

            return {'code': 200, 'status': True,
                    'response': {'id': mymodel.id,
                                 'name': mymodel.name,
                                 'value': mymodel.value}}

        except Exception as e:
            return {'code': 500, 'status': False, 'response': str(e)}

    def delete(self):
        """
        刪除 API
        """
        try:
            mymodel = self.dbsession.query(MyModel)\
                          .filter(MyModel.id == int(self.request.matchdict['id']))\
                          .first()
            with transaction.manager:
                self.dbsession.delete(mymodel)
                self.dbsession.flush()

            return {'code': 200, 'status': True,
                    'response': 'OK'}

        except Exception as e:
            return {'code': 501, 'status': False, 'response': str(e)}

