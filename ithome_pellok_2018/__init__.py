import os
from pyramid.config import Configurator
from pyramid_redis_sessions import session_factory_from_settings

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    
    # postgres db config
    if os.environ.get('DB_HOST'):
        db_setting = dict(host=os.environ.get('DB_HOST'),
                          port=int(os.environ.get('DB_PORT')),
                          user=os.environ.get('DB_USER'),
                          passwd=os.environ.get('DB_PASS'),
                          name=os.environ.get('DB_NAME')
                          )
        DB_FMT_STR = 'postgresql+psycopg2://{user}:{passwd}@{host}:{port}/{name}'
        settings['sqlalchemy.url'] = DB_FMT_STR.format(**db_setting)

    # redis db config
    if os.environ.get('REDIS_HOST'):
        settings['redis.sessions.host'] = os.environ.get('REDIS_HOST')
        settings['redis.sessions.port'] = os.environ.get('REDIS_PORT')

    session_factory = session_factory_from_settings(settings)
    config = Configurator(settings=settings,
                          session_factory=session_factory)
    config.include('pyramid_jinja2')
    config.include('.models')
    config.include('.views')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.scan()
    return config.make_wsgi_app()
